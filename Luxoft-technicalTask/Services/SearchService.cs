﻿using DataAccess;
using DataAccess.Model;
using EFCore.BulkExtensions;
using Luxoft_technicalTask.DTOs;
using Luxoft_technicalTask.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace Luxoft_technicalTask.Services
{
    public class SearchService
    {
        private int batchNumber = 1;
        private int batchSize;
        private string csvSeparator;
        private LuxoftContext _context;
        private Dictionary<string, int> Properties;

        /// <summary>
        /// All existing assets in the database
        /// </summary>
        private Dictionary<int, Asset> AssetsDb;
        /// <summary>
        /// New asstes incoming from file that are going to be added
        /// </summary>
        private Dictionary<int, Asset> AssetsNew;

        private List<Asset> AssetsToAdd = new List<Asset>();

        /// <summary>
        /// All existing AssetsProperty in database
        /// </summary>
        private Dictionary<int, AssetProperty> AssetsPropertyDb;
        /// <summary>
        /// New assets properties from file that are going to be updated in db
        /// </summary>
        private Dictionary<int, AssetProperty> AssetsPropertyNew;

        private List<AssetProperty> AssetsPropertyToAdd = new List<AssetProperty>();
        private List<AssetProperty> AssetsPropertyToUpdate = new List<AssetProperty>();

        private bool hasError;
        private List<string> Errors = new List<string>();

        private Dictionary<string, List<string>> returnInfo = new Dictionary<string, List<string>>();

        private List<Task> pendingSaves = new List<Task>();

        public SearchService(LuxoftContext context, IConfiguration configuration)
        {
            batchSize = int.Parse(configuration.GetSection("BatchSize").Value);
            csvSeparator = configuration.GetSection("CsvSeparator").Value;
            _context = context;
            Properties = _context.Properties.ToDictionary(p => p.Description, p => p.Id);

            AssetsDb = _context.Assets.ToDictionary(p => p.Id, a=>a);
            AssetsNew = new Dictionary<int, Asset>();

            AssetsPropertyDb = _context.AssetProperties.ToDictionary(p => GeneralId(p.AssetId, p.PropertyId), a => a);
            AssetsPropertyNew = new Dictionary<int, AssetProperty>();
        }

        /// <summary>
        /// Inserting new records from file
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public async Task<ServiceResponse<Dictionary<string, List<string>>>> UploadFromFile(IFormFile file)
        {
            if (file == null)
                return new ServiceResponse<Dictionary<string, List<string>>>(new BadRequestObjectResult("Missing File"));
            int lineNbr = 0;
            using (StreamReader sr = new StreamReader(file.OpenReadStream()))
            {
                string singleLine;
                while ((singleLine = sr.ReadLine()) != null)
                {
                    if (!string.IsNullOrWhiteSpace(singleLine))
                    {
                        var assetDto = ParseLine(singleLine, lineNbr++);
                        if (assetDto != null)
                            CheckAnQueueSave(assetDto);
                    }
                }
            }
            
            await Task.WhenAll(pendingSaves);
            await MoveAndBulkSave(); //Flush

            return new ServiceResponse<Dictionary<string, List<string>>>(returnInfo);
        }

        /// <summary>
        /// Look for assets Id's by property name and value
        /// </summary>
        /// <param name="propertyValueDto">Property name and value</param>
        /// <returns>List of assets Id's</returns>
        public async Task<ServiceResponse<List<int>>> SearchByProperyValue(PropertyValueDto propertyValueDto)
        {
            var propId = GetPropertyId(propertyValueDto.Property);
            if (hasError)
                return new ServiceResponse<List<int>>(new BadRequestObjectResult(Errors));
            
            var assets = await _context.AssetProperties
                .Where(a => a.PropertyId == propId && a.Value == bool.Parse(propertyValueDto.Value))
                .Select(a => a.AssetId)
                .ToListAsync();

            return new ServiceResponse<List<int>>(assets);
        }

        /// <summary>
        /// Updates single record, according to timestamp logic
        /// </summary>
        /// <param name="incomingAsset"></param>
        /// <returns></returns>
        public async Task<ServiceResponse<Dictionary<string, List<string>>>> SingleUpsert(IncomingAssetDto incomingAsset)
        {
            CheckAnQueueSave(incomingAsset);
            await MoveAndBulkSave(); //Flush
            if (hasError)
                return new ServiceResponse<Dictionary<string, List<string>>>(new BadRequestObjectResult(Errors));
            return new ServiceResponse<Dictionary<string, List<string>>>(returnInfo);
        }

        #region privateMethods
        private IncomingAssetDto ParseLine(string line, int lineNumber)
        {
            /*
             * AssetId,Properties,Value,Timestamp
             * 1,”is cash”,”true”,” 2020-07-01T16:32:32”
             */

            var items = line.Split(csvSeparator);

            if(items.Length!=4)
            {
                string info = $"Line {lineNumber} has wrong number of paramiters {items.Length}";
                Log.Information(info);
                AddReturnInfo("Bad input", info);
                return null;
            }

            string[] cleanedItems = new string[4];

            for (int i = 0; i < items.Length; i++)
                cleanedItems[i] = items[i].Replace("\\\"", string.Empty).Replace("\"", string.Empty);

            int assetId;
            int property;
            bool value;
            DateTime timestamp;


            if (!int.TryParse(cleanedItems[0], out assetId)
                || !Properties.TryGetValue(cleanedItems[1], out property)
                || !bool.TryParse(cleanedItems[2], out value)
                || !DateTime.TryParse(cleanedItems[3].Trim(), out timestamp))
            {
                string info = $"Line {lineNumber} has properties that could not be parsed. Oryginal properties: {line}";
                Log.Information(info);
                AddReturnInfo("Bad input", info);
                return null;
            }
            
            var asset = new IncomingAssetDto();
            asset.AssetId = assetId;
            asset.Property = cleanedItems[1];
            asset.Value = value.ToString();
            asset.Timestamp = timestamp.ToShortTimeString();

            return asset;
        }



        /// <summary>
        /// Main loop for inserting records
        /// </summary>
        /// <param name="incomingAssets"></param>
        /// <returns></returns>
        private void CheckAnQueueSave(IncomingAssetDto incomingAsset)
        {
            CheckAssetExistance(incomingAsset);
            CheckProperty(incomingAsset);

            if(AssetsPropertyToAdd.Count + AssetsPropertyToUpdate.Count >= batchSize)
            {
                pendingSaves.Add(MoveAndBulkSave());
            }

        }

        private async Task MoveAndBulkSave()
        {
            int batchNum = batchNumber++;
            Log.Information("Saving Batch no. {0} ....", batchNum);
            
            var assetsToAdd = MoveList<Asset>(AssetsToAdd);
            var propertiesToAdd = MoveList<AssetProperty>(AssetsPropertyToAdd);
            var propertiesToUpdate = MoveList<AssetProperty>(AssetsPropertyToUpdate);
            
            await BulkSaveChanges(assetsToAdd, propertiesToUpdate, propertiesToAdd);
            Log.Information("Batch Save no. {0} Finished", batchNum);
        }

        /// <summary>
        /// Validate if asset exist and if nessecary add to collection
        /// </summary>
        /// <param name="incomingAssetDto"></param>
        /// <returns></returns>
        private void CheckAssetExistance(IncomingAssetDto incomingAssetDto)
        {
            if(!(AssetsDb.ContainsKey(incomingAssetDto.AssetId) || AssetsNew.ContainsKey(incomingAssetDto.AssetId)))
            {
                Log.Information("Asset with Id {0} does not exist in the database. It will be created", incomingAssetDto.AssetId);
                Asset newAsset = new Asset()
                {
                    Id = incomingAssetDto.AssetId
                };
                newAsset.WhenCreating();

                AssetsNew.Add(incomingAssetDto.AssetId, newAsset);
                AssetsToAdd.Add(newAsset);
            }
        }

        /// <summary>
        /// Validate if property should be updated in db. If so adds to collection
        /// </summary>
        /// <param name="incomingAssetDto"></param>
        private void CheckProperty(IncomingAssetDto incomingAssetDto)
        {
            int propertyId = GetPropertyId(incomingAssetDto.Property);
            if (propertyId!=0)
            {
                bool isTotallyNew = false;
                int assetGeneralId = GeneralId(incomingAssetDto.AssetId, propertyId);
                DateTime assetTimestamp = DateTime.Parse(incomingAssetDto.Timestamp);

                AssetProperty existingAssetProperty;
                AssetProperty newAssetProperty;
                if (!AssetsPropertyDb.TryGetValue(assetGeneralId, out existingAssetProperty)) //Looking in db
                    if (!AssetsPropertyNew.TryGetValue(assetGeneralId, out existingAssetProperty)) //looking in current import file
                    {
                        //totally new property - creation
                        newAssetProperty = new AssetProperty();
                        newAssetProperty.AssetId = incomingAssetDto.AssetId;
                        newAssetProperty.PropertyId = propertyId;
                        newAssetProperty.Value = bool.Parse(incomingAssetDto.Value);
                        newAssetProperty.TimeStamp = DateTime.Parse(incomingAssetDto.Timestamp);

                        newAssetProperty.WhenCreating();
                        isTotallyNew = true;

                        //add to lists
                        AssetsPropertyNew.Add(assetGeneralId, newAssetProperty);
                        AssetsPropertyToAdd.Add(newAssetProperty);

                        //New log to response
                        var info = string.Format("Asset {0} with property {1} was created", incomingAssetDto.AssetId, incomingAssetDto.Property);
                        AddReturnInfo("Created assets", info);
                    }
            
                //Condition check - timestamp in db older then incoming
                if (!isTotallyNew)
                {
                    if(DateTime.Compare(existingAssetProperty.TimeStamp, assetTimestamp)<=0)
                    {
                        existingAssetProperty.Value = bool.Parse(incomingAssetDto.Value);
                        existingAssetProperty.TimeStamp = assetTimestamp;

                        AssetsPropertyToUpdate.Add(existingAssetProperty);
                        //Update log to response
                        var info = string.Format("Asset {0} with property {1} was updated", incomingAssetDto.AssetId, incomingAssetDto.Property);
                        AddReturnInfo("Updated assets", info);
                    }
                    else
                    {
                        var info = string.Format("Asset {0} with property {1} was not changed because in DB is newer record", incomingAssetDto.AssetId, incomingAssetDto.Property);
                        Log.Information(info);
                        //Not Updated log to response
                        AddReturnInfo("Not updated assets", info);
                    }
                }
            }
        }

        private async Task BulkSaveChanges(List<Asset> assetsToAdd, List<AssetProperty> toUpdate, List<AssetProperty> toAdd)
        {
            BulkConfig bulkConfig = new BulkConfig() { TrackingEntities = false };
            try
            {
                await _context.BulkInsertAsync<Asset>(assetsToAdd, bulkConfig);
                await _context.BulkInsertAsync<AssetProperty>(toAdd, bulkConfig);
                await _context.BulkUpdateAsync<AssetProperty>(toUpdate, bulkConfig);
                
                Log.Information("Bulk upser done \t Assets added: {0} \t Properties Added: {1} \t Properties Updated: {2}", assetsToAdd.Count, toAdd.Count, toUpdate.Count);
            }
            catch (Exception ex)
            {
                hasError = true;
                Errors.Add(ex.Message);
                Log.Error(ex.Message);
            }


        }

        private int GetPropertyId(string propertyName)
        {
            int propertyId;
            if (!Properties.TryGetValue(propertyName, out propertyId))
            {
                Errors.Add(string.Format("Can't find property {0} in Db Master Data", propertyName));
                hasError = true;
                propertyId = 0;
            }

            return propertyId;
        }

        private int GeneralId(int assetId, int propertyId)
        {
            return HashCode.Combine(assetId, propertyId);
        }

        private List<T> MoveList<T>(List<T> incomingList)
        {
            List<T> newList = new List<T>();
            newList.AddRange(incomingList);
            incomingList.RemoveRange(0, incomingList.Count);

            return newList;
        }

        private void AddReturnInfo(string category, string info)
        {
            if (returnInfo.ContainsKey(category))
                returnInfo[category].Add(info);
            else
                returnInfo.Add(category, new List<string>() { info });
        }
        #endregion
    }
}
