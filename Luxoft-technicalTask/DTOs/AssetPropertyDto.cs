﻿using DataAccess.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Luxoft_technicalTask.DTOs
{
    //Response
    public class AssetPropertyDto
    {
        public int AssetId { get; set; }
        public string Description { get; set; }
        public bool Value { get; set; }
        public DateTime Timestamp { get; set; }

        public AssetPropertyDto()
        {

        }

        public AssetPropertyDto(AssetProperty assetProperty)
        {
            AssetId = assetProperty.Id;
            if(assetProperty.Property!=null)
                Description = assetProperty.Property.Description;
            Value = assetProperty.Value;
            Timestamp = assetProperty.TimeStamp;
        }


    }
}
