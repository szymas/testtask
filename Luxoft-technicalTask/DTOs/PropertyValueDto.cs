﻿using Luxoft_technicalTask.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Luxoft_technicalTask.DTOs
{
    public class PropertyValueDto
    {
        [Required]
        public string Property { get; set; }
        [Required]
        [BoolStringValidation("Value has to be \"true\" or \"false\"")]
        public string Value { get; set; }

        public PropertyValueDto()
        {

        }


    }
}
