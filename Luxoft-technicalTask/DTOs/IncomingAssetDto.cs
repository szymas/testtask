﻿using Luxoft_technicalTask.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Luxoft_technicalTask.DTOs
{
    public class IncomingAssetDto
    {
        [Required]
        public int AssetId { get; set; }
        [Required]
        public string Property { get; set; }
        [Required]
        [BoolStringValidation("Value has to be \"true\" or \"false\"")]
        public string Value { get; set; }
        [Required]
        [Timestamp]
        public string Timestamp { get; set; }


        public IncomingAssetDto()
        {

        }

    }
}
