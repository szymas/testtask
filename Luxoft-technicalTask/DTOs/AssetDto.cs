﻿using DataAccess.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Luxoft_technicalTask.DTOs
{
    //Responce
    public class AssetDto
    {
        public int Id { get; set; }
        public List<AssetPropertyDto> Properties { get; set; }

        public AssetDto()
        {

        }

        public AssetDto(Asset asset)
        {
            Id = asset.Id;
            Properties = new List<AssetPropertyDto>();
            if (asset.Properties != null)
            {
                foreach (var prop in asset.Properties)
                {
                    Properties.Add(new AssetPropertyDto(prop));
                }
            }
        }
    }
}
