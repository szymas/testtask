﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Luxoft_technicalTask.Helpers
{
    public class ServiceResponse<T>
    {
        //If all OK response object
        public T Response { get; private set; }

        //If Error or unauthorize 
        public ActionResult ErrorObjectResult { get; private set; }


        public bool HasError { get { if (ErrorObjectResult != null) return true; else return false; } }

        public ServiceResponse()
        {

        }

        public ServiceResponse(T resp)
        {
            Response = resp;
        }

        public ServiceResponse(ActionResult errorObjectResult)
        {
            ErrorObjectResult = errorObjectResult;
        }

        public IActionResult GetResponse()
        {

            if (HasError)
                return ErrorObjectResult;
            else
            {
                if (Response != null)
                    return new OkObjectResult(Response);
                else
                    return new OkResult();
            }

        }

        public object GetErrorDesc()
        {
            if (ErrorObjectResult != null)
            {
                if (ErrorObjectResult is ObjectResult)
                    return (ErrorObjectResult as ObjectResult).Value;
            }
            return null;
        }
    }
}
