﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Luxoft_technicalTask.Helpers
{
    public class BoolStringValidation : ValidationAttribute
    {
        public BoolStringValidation()
        {
        }

        public BoolStringValidation(Func<string> errorMessageAccessor) : base(errorMessageAccessor)
        {
        }

        public BoolStringValidation(string errorMessage) : base(errorMessage)
        {
        }

        public override bool IsValid(object value)
        {
            var acceptedValues = new List<string>() { "true", "false"};
            if (acceptedValues.Contains(value.ToString().ToLower()))
                return true;
            return false;
        }

        
    }
}
