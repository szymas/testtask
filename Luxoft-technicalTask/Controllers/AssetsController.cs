﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Luxoft_technicalTask.DTOs;
using Luxoft_technicalTask.Helpers;
using Luxoft_technicalTask.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace Luxoft_technicalTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AssetsController : ControllerBase
    {
        private SearchService _searchService;

        public AssetsController(SearchService searchService)
        {
            _searchService = searchService;
        }

        [HttpPost("File")]
        public async Task<IActionResult> AddFromFile([FromForm] IFormFile file)
        {
            var resp = await _searchService.UploadFromFile(file);
            if (!resp.HasError)
            {
                StringBuilder summary = new StringBuilder();
                foreach (var key in resp.Response.Keys)
                    summary.AppendLine($"{key}: {resp.Response[key].Count}");

                return Ok(new { Summary = summary.ToString(), Infos = resp.Response });
            }
            return resp.GetResponse();
        }


        [HttpPost("search")]
        public async Task<IActionResult> SearchByProperty([FromBody] PropertyValueDto propertyValueDto)
        {
            var resp = await _searchService.SearchByProperyValue(propertyValueDto);
            return resp.GetResponse();
        }

        [HttpPost("singleupsert")]
        public async Task<IActionResult> SingleUpsert([FromBody] IncomingAssetDto incomingAssetDto)
        {
            var resp = await _searchService.SingleUpsert(incomingAssetDto);
            return resp.GetResponse();
        }
    }
}
