﻿using DataAccess.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Configurations
{
    class PropertyConfiguration : IEntityTypeConfiguration<Property>
    {
        public void Configure(EntityTypeBuilder<Property> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.HasIndex(p => p.Description).IsUnique();

            builder.Property(p => p.Description).HasMaxLength(50);

            var initialValues = new List<string>() { "is fix income", "is convertible", "is swap", "is cash", "is future" };

            int id = 0;
            foreach (var item in initialValues)
            {
                Property prop = new Property() {Id=++id, Description = item };
                prop.WhenCreating();
                builder.HasData(prop);
            }

        }
    }
}
