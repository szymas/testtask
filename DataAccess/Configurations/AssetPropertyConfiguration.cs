﻿using DataAccess.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Configurations
{
    class AssetPropertyConfiguration : IEntityTypeConfiguration<AssetProperty>
    {
        public void Configure(EntityTypeBuilder<AssetProperty> builder)
        {
            builder.Property(p => p.Value).HasDefaultValue(false);

            builder.HasOne(p => p.Property)
                .WithMany(p => p.AssetProperties)
                .HasForeignKey(p => p.PropertyId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Property(p => p.TimeStamp).HasDefaultValue(DateTime.MinValue);
        }
    }
}
