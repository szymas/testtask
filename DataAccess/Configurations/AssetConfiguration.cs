﻿using DataAccess.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Configurations
{
    class AssetConfiguration : IEntityTypeConfiguration<Asset>
    {
        public void Configure(EntityTypeBuilder<Asset> builder)
        {
            builder.Property(p => p.Id).ValueGeneratedNever();

            builder.HasMany(p => p.Properties)
                .WithOne(p => p.Asset)
                .HasForeignKey(p => p.AssetId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
