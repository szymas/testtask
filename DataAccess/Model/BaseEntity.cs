﻿using Microsoft.EntityFrameworkCore.Migrations.Operations.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Model
{
    /// <summary>
    /// Base class for all entities in model. 
    /// In future could be exdended for creator and modificator userId...
    /// </summary>
    public class BaseEntity
    {
        public int Id { get; set; }
        public DateTime CreatedTS { get; set; }
        public DateTime ModifiedTS { get; set; }


        /// <summary>
        /// Aktualizuje CreatedTS = UtcNow + CreatedUserId
        /// </summary>
        public void WhenCreating()
        {
            CreatedTS = DateTime.UtcNow;

            WhenModify();
        }

        /// <summary>
        /// Aktualizuje ModifiedTS = UtcNow + ModifiedUserId
        /// </summary>
        public void WhenModify()
        {
            ModifiedTS = DateTime.UtcNow;
        }
    }
}



