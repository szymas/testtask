﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Model
{
    public class AssetProperty:BaseEntity
    {
        public int AssetId { get; set; }
        public int PropertyId { get; set; }
        public bool Value { get; set; }
        /// <summary>
        /// Asset TS from file
        /// </summary>
        public DateTime TimeStamp { get; set; }


        public Property Property { get; set; }
        public Asset Asset { get; set; }


    }
}
