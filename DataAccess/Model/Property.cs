﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Model
{
    public class Property:BaseEntity
    {
        public string Description { get; set; }

        public IEnumerable<AssetProperty> AssetProperties { get; set; }
    }
}
