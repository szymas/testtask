﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class nameConvention : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AssetProperty_Asset_AssetId",
                table: "AssetProperty");

            migrationBuilder.DropForeignKey(
                name: "FK_AssetProperty_Properties_PropertyId",
                table: "AssetProperty");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AssetProperty",
                table: "AssetProperty");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Asset",
                table: "Asset");

            migrationBuilder.RenameTable(
                name: "AssetProperty",
                newName: "AssetProperties");

            migrationBuilder.RenameTable(
                name: "Asset",
                newName: "Assets");

            migrationBuilder.RenameIndex(
                name: "IX_AssetProperty_PropertyId",
                table: "AssetProperties",
                newName: "IX_AssetProperties_PropertyId");

            migrationBuilder.RenameIndex(
                name: "IX_AssetProperty_AssetId",
                table: "AssetProperties",
                newName: "IX_AssetProperties_AssetId");

            migrationBuilder.AlterColumn<bool>(
                name: "Value",
                table: "AssetProperties",
                nullable: false,
                defaultValue: false,
                oldClrType: typeof(bool),
                oldType: "bit");

            migrationBuilder.AlterColumn<DateTime>(
                name: "TimeStamp",
                table: "Assets",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AssetProperties",
                table: "AssetProperties",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Assets",
                table: "Assets",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 12, 10, 46, 16, 440, DateTimeKind.Utc).AddTicks(1598), new DateTime(2020, 10, 12, 10, 46, 16, 440, DateTimeKind.Utc).AddTicks(2252) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 12, 10, 46, 16, 441, DateTimeKind.Utc).AddTicks(501), new DateTime(2020, 10, 12, 10, 46, 16, 441, DateTimeKind.Utc).AddTicks(518) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 12, 10, 46, 16, 441, DateTimeKind.Utc).AddTicks(625), new DateTime(2020, 10, 12, 10, 46, 16, 441, DateTimeKind.Utc).AddTicks(626) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 12, 10, 46, 16, 441, DateTimeKind.Utc).AddTicks(629), new DateTime(2020, 10, 12, 10, 46, 16, 441, DateTimeKind.Utc).AddTicks(630) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 12, 10, 46, 16, 441, DateTimeKind.Utc).AddTicks(632), new DateTime(2020, 10, 12, 10, 46, 16, 441, DateTimeKind.Utc).AddTicks(633) });

            migrationBuilder.AddForeignKey(
                name: "FK_AssetProperties_Assets_AssetId",
                table: "AssetProperties",
                column: "AssetId",
                principalTable: "Assets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AssetProperties_Properties_PropertyId",
                table: "AssetProperties",
                column: "PropertyId",
                principalTable: "Properties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AssetProperties_Assets_AssetId",
                table: "AssetProperties");

            migrationBuilder.DropForeignKey(
                name: "FK_AssetProperties_Properties_PropertyId",
                table: "AssetProperties");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Assets",
                table: "Assets");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AssetProperties",
                table: "AssetProperties");

            migrationBuilder.RenameTable(
                name: "Assets",
                newName: "Asset");

            migrationBuilder.RenameTable(
                name: "AssetProperties",
                newName: "AssetProperty");

            migrationBuilder.RenameIndex(
                name: "IX_AssetProperties_PropertyId",
                table: "AssetProperty",
                newName: "IX_AssetProperty_PropertyId");

            migrationBuilder.RenameIndex(
                name: "IX_AssetProperties_AssetId",
                table: "AssetProperty",
                newName: "IX_AssetProperty_AssetId");

            migrationBuilder.AlterColumn<DateTime>(
                name: "TimeStamp",
                table: "Asset",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AlterColumn<bool>(
                name: "Value",
                table: "AssetProperty",
                type: "bit",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValue: false);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Asset",
                table: "Asset",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AssetProperty",
                table: "AssetProperty",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 12, 10, 41, 22, 646, DateTimeKind.Utc).AddTicks(6807), new DateTime(2020, 10, 12, 10, 41, 22, 646, DateTimeKind.Utc).AddTicks(7420) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 12, 10, 41, 22, 647, DateTimeKind.Utc).AddTicks(5408), new DateTime(2020, 10, 12, 10, 41, 22, 647, DateTimeKind.Utc).AddTicks(5421) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 12, 10, 41, 22, 647, DateTimeKind.Utc).AddTicks(5482), new DateTime(2020, 10, 12, 10, 41, 22, 647, DateTimeKind.Utc).AddTicks(5483) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 12, 10, 41, 22, 647, DateTimeKind.Utc).AddTicks(5485), new DateTime(2020, 10, 12, 10, 41, 22, 647, DateTimeKind.Utc).AddTicks(5486) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 12, 10, 41, 22, 647, DateTimeKind.Utc).AddTicks(5488), new DateTime(2020, 10, 12, 10, 41, 22, 647, DateTimeKind.Utc).AddTicks(5489) });

            migrationBuilder.AddForeignKey(
                name: "FK_AssetProperty_Asset_AssetId",
                table: "AssetProperty",
                column: "AssetId",
                principalTable: "Asset",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AssetProperty_Properties_PropertyId",
                table: "AssetProperty",
                column: "PropertyId",
                principalTable: "Properties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
