﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class TimestampToProperty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TimeStamp",
                table: "Assets");

            migrationBuilder.AddColumn<DateTime>(
                name: "TimeStamp",
                table: "AssetProperties",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(79), new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(791) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(8967), new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(8984) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(9055), new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(9056) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(9060), new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(9061) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(9064), new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(9065) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TimeStamp",
                table: "AssetProperties");

            migrationBuilder.AddColumn<DateTime>(
                name: "TimeStamp",
                table: "Assets",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 12, 10, 46, 16, 440, DateTimeKind.Utc).AddTicks(1598), new DateTime(2020, 10, 12, 10, 46, 16, 440, DateTimeKind.Utc).AddTicks(2252) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 12, 10, 46, 16, 441, DateTimeKind.Utc).AddTicks(501), new DateTime(2020, 10, 12, 10, 46, 16, 441, DateTimeKind.Utc).AddTicks(518) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 12, 10, 46, 16, 441, DateTimeKind.Utc).AddTicks(625), new DateTime(2020, 10, 12, 10, 46, 16, 441, DateTimeKind.Utc).AddTicks(626) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 12, 10, 46, 16, 441, DateTimeKind.Utc).AddTicks(629), new DateTime(2020, 10, 12, 10, 46, 16, 441, DateTimeKind.Utc).AddTicks(630) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 12, 10, 46, 16, 441, DateTimeKind.Utc).AddTicks(632), new DateTime(2020, 10, 12, 10, 46, 16, 441, DateTimeKind.Utc).AddTicks(633) });
        }
    }
}
