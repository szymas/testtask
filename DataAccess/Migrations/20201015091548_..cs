﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class _ : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 15, 9, 15, 48, 171, DateTimeKind.Utc).AddTicks(3513), new DateTime(2020, 10, 15, 9, 15, 48, 171, DateTimeKind.Utc).AddTicks(4131) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 15, 9, 15, 48, 172, DateTimeKind.Utc).AddTicks(2134), new DateTime(2020, 10, 15, 9, 15, 48, 172, DateTimeKind.Utc).AddTicks(2148) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 15, 9, 15, 48, 172, DateTimeKind.Utc).AddTicks(2207), new DateTime(2020, 10, 15, 9, 15, 48, 172, DateTimeKind.Utc).AddTicks(2207) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 15, 9, 15, 48, 172, DateTimeKind.Utc).AddTicks(2210), new DateTime(2020, 10, 15, 9, 15, 48, 172, DateTimeKind.Utc).AddTicks(2211) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 15, 9, 15, 48, 172, DateTimeKind.Utc).AddTicks(2213), new DateTime(2020, 10, 15, 9, 15, 48, 172, DateTimeKind.Utc).AddTicks(2214) });

            migrationBuilder.CreateIndex(
                name: "IX_AssetProperties_AssetId",
                table: "AssetProperties",
                column: "AssetId");

            migrationBuilder.AddForeignKey(
                name: "FK_AssetProperties_Assets_AssetId",
                table: "AssetProperties",
                column: "AssetId",
                principalTable: "Assets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AssetProperties_Assets_AssetId",
                table: "AssetProperties");

            migrationBuilder.DropIndex(
                name: "IX_AssetProperties_AssetId",
                table: "AssetProperties");

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 15, 9, 7, 46, 59, DateTimeKind.Utc).AddTicks(3173), new DateTime(2020, 10, 15, 9, 7, 46, 59, DateTimeKind.Utc).AddTicks(3799) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 15, 9, 7, 46, 60, DateTimeKind.Utc).AddTicks(1890), new DateTime(2020, 10, 15, 9, 7, 46, 60, DateTimeKind.Utc).AddTicks(1903) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 15, 9, 7, 46, 60, DateTimeKind.Utc).AddTicks(1987), new DateTime(2020, 10, 15, 9, 7, 46, 60, DateTimeKind.Utc).AddTicks(1988) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 15, 9, 7, 46, 60, DateTimeKind.Utc).AddTicks(1991), new DateTime(2020, 10, 15, 9, 7, 46, 60, DateTimeKind.Utc).AddTicks(1991) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 15, 9, 7, 46, 60, DateTimeKind.Utc).AddTicks(1994), new DateTime(2020, 10, 15, 9, 7, 46, 60, DateTimeKind.Utc).AddTicks(1994) });
        }
    }
}
