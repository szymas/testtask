﻿// <auto-generated />
using System;
using DataAccess;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace DataAccess.Migrations
{
    [DbContext(typeof(LuxoftContext))]
    [Migration("20201014183411_TimestampToProperty")]
    partial class TimestampToProperty
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.8")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DataAccess.Model.Asset", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedTS")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("ModifiedTS")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.ToTable("Assets");
                });

            modelBuilder.Entity("DataAccess.Model.AssetProperty", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("AssetId")
                        .HasColumnType("int");

                    b.Property<DateTime>("CreatedTS")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("ModifiedTS")
                        .HasColumnType("datetime2");

                    b.Property<int>("PropertyId")
                        .HasColumnType("int");

                    b.Property<DateTime>("TimeStamp")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("datetime2")
                        .HasDefaultValue(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

                    b.Property<bool>("Value")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bit")
                        .HasDefaultValue(false);

                    b.HasKey("Id");

                    b.HasIndex("AssetId");

                    b.HasIndex("PropertyId");

                    b.ToTable("AssetProperties");
                });

            modelBuilder.Entity("DataAccess.Model.Property", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedTS")
                        .HasColumnType("datetime2");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<DateTime>("ModifiedTS")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.HasIndex("Description")
                        .IsUnique()
                        .HasFilter("[Description] IS NOT NULL");

                    b.ToTable("Properties");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CreatedTS = new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(79),
                            Description = "is fix income",
                            ModifiedTS = new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(791)
                        },
                        new
                        {
                            Id = 2,
                            CreatedTS = new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(8967),
                            Description = "is convertible",
                            ModifiedTS = new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(8984)
                        },
                        new
                        {
                            Id = 3,
                            CreatedTS = new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(9055),
                            Description = "is swap",
                            ModifiedTS = new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(9056)
                        },
                        new
                        {
                            Id = 4,
                            CreatedTS = new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(9060),
                            Description = "is cash",
                            ModifiedTS = new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(9061)
                        },
                        new
                        {
                            Id = 5,
                            CreatedTS = new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(9064),
                            Description = "is future",
                            ModifiedTS = new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(9065)
                        });
                });

            modelBuilder.Entity("DataAccess.Model.AssetProperty", b =>
                {
                    b.HasOne("DataAccess.Model.Asset", "Asset")
                        .WithMany("Properties")
                        .HasForeignKey("AssetId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DataAccess.Model.Property", "Property")
                        .WithMany("AssetProperties")
                        .HasForeignKey("PropertyId")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
