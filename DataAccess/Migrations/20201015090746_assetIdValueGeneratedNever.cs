﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class assetIdValueGeneratedNever : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AssetProperties_Assets_AssetId",
                table: "AssetProperties");

            migrationBuilder.DropIndex(
                name: "IX_AssetProperties_AssetId",
                table: "AssetProperties");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Assets",
                table:"Assets");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "Assets");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "Assets",
                nullable: false);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Assets",
                table: "Assets",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 15, 9, 7, 46, 59, DateTimeKind.Utc).AddTicks(3173), new DateTime(2020, 10, 15, 9, 7, 46, 59, DateTimeKind.Utc).AddTicks(3799) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 15, 9, 7, 46, 60, DateTimeKind.Utc).AddTicks(1890), new DateTime(2020, 10, 15, 9, 7, 46, 60, DateTimeKind.Utc).AddTicks(1903) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 15, 9, 7, 46, 60, DateTimeKind.Utc).AddTicks(1987), new DateTime(2020, 10, 15, 9, 7, 46, 60, DateTimeKind.Utc).AddTicks(1988) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 15, 9, 7, 46, 60, DateTimeKind.Utc).AddTicks(1991), new DateTime(2020, 10, 15, 9, 7, 46, 60, DateTimeKind.Utc).AddTicks(1991) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 15, 9, 7, 46, 60, DateTimeKind.Utc).AddTicks(1994), new DateTime(2020, 10, 15, 9, 7, 46, 60, DateTimeKind.Utc).AddTicks(1994) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Id",
                table: "Assets",
                type: "int",
                nullable: false,
                oldClrType: typeof(int))
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(79), new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(791) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(8967), new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(8984) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(9055), new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(9056) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(9060), new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(9061) });

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedTS", "ModifiedTS" },
                values: new object[] { new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(9064), new DateTime(2020, 10, 14, 18, 34, 10, 800, DateTimeKind.Utc).AddTicks(9065) });

            migrationBuilder.CreateIndex(
                name: "IX_AssetProperties_AssetId",
                table: "AssetProperties",
                column: "AssetId");

            migrationBuilder.AddForeignKey(
                name: "FK_AssetProperties_Assets_AssetId",
                table: "AssetProperties",
                column: "AssetId",
                principalTable: "Assets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
