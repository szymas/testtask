﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class initalValues : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Properties",
                columns: new[] { "Id", "CreatedTS", "Description", "ModifiedTS" },
                values: new object[,]
                {
                    { 1, new DateTime(2020, 10, 12, 10, 41, 22, 646, DateTimeKind.Utc).AddTicks(6807), "is fix income", new DateTime(2020, 10, 12, 10, 41, 22, 646, DateTimeKind.Utc).AddTicks(7420) },
                    { 2, new DateTime(2020, 10, 12, 10, 41, 22, 647, DateTimeKind.Utc).AddTicks(5408), "is convertible", new DateTime(2020, 10, 12, 10, 41, 22, 647, DateTimeKind.Utc).AddTicks(5421) },
                    { 3, new DateTime(2020, 10, 12, 10, 41, 22, 647, DateTimeKind.Utc).AddTicks(5482), "is swap", new DateTime(2020, 10, 12, 10, 41, 22, 647, DateTimeKind.Utc).AddTicks(5483) },
                    { 4, new DateTime(2020, 10, 12, 10, 41, 22, 647, DateTimeKind.Utc).AddTicks(5485), "is cash", new DateTime(2020, 10, 12, 10, 41, 22, 647, DateTimeKind.Utc).AddTicks(5486) },
                    { 5, new DateTime(2020, 10, 12, 10, 41, 22, 647, DateTimeKind.Utc).AddTicks(5488), "is future", new DateTime(2020, 10, 12, 10, 41, 22, 647, DateTimeKind.Utc).AddTicks(5489) }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 5);
        }
    }
}
