﻿using DataAccess.Configurations;
using DataAccess.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess
{
    public class LuxoftContext:DbContext
    {
        public DbSet<Property> Properties { get; set; }
        public DbSet<AssetProperty> AssetProperties { get; set; }
        public DbSet<Asset> Assets { get; set; }

        public LuxoftContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new PropertyConfiguration());
            modelBuilder.ApplyConfiguration(new AssetPropertyConfiguration());
            modelBuilder.ApplyConfiguration(new AssetConfiguration());
            
        }
    }
}
